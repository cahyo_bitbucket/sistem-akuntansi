<?php
	class Kasmasuk extends CI_Controller{
		public $data;
		public $config_upload_foto;
		public function __construct(){
			parent::__construct();
			$this->data = array();
			$this->data['content'] = "Kasmasuk";
			$this->data['module'] = "kasmasuk";
			$this->load->model('Mkasmasuk');
			$this->load->model('Mproducts');
			$this->data['dataproduct'] = $this->Mproducts->get_products();
			$this->data['date'] = tgl_indo(date('Y-m-d'));
		}

		public function index(){
			$kasmasuk_date = date('Y-m-d');
			$this->data['datakasmasuk'] = $this->Mkasmasuk->get_kasmasuk($kasmasuk_date);
			$this->load->view('index',$this->data);
		}

		public function add(){
			if($this->input->post()){
					$data = $this->input->post();
					$data['user_id'] = $this->data['user_id'];

					//dapatkan stock product sebelumnya
					$this->load->model('Mproducts');
					$qty_old_product = $this->Mproducts->get_product($data['product']);

					$this->Mkasmasuk->add($data,$qty_old_product);
					redirect($this->data['module']);
			}

			$this->data['proses'] = "add";
			$this->data['action_form'] = site_url($this->data['module']."/add");
			$this->load->view('index',$this->data);
		}
		public function update($id){
			if($this->input->post()){
					$data = $this->input->post();
					$data['user_id'] = $this->data['user_id'];
					$this->Mkasmasuk->update($data,$id);
					redirect($this->data['module']);
				}

				$update = $this->Mkasmasuk->get_masuk($id);

				$this->data['action_form'] = site_url($this->data['module']."/update/".$id);
				$this->data['databuys'] = $update;
				$this->data['proses'] = "update";
				$this->load->view('index',$this->data);
		}

		public function delete($id){
			$this->Mkasmasuk->delete($id);
			redirect('kasmasuk');
		}
	}
