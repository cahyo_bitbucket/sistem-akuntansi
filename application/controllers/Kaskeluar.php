<?php
	class Kaskeluar extends CI_Controller{
		public $data;
		public $config_upload_foto;
		public function __construct(){
			parent::__construct();
			$this->data = array();
			$this->data['content'] = "Kaskeluar";
			$this->data['module'] = "Kaskeluar";
			$this->load->model('Mkaskeluar');
		}

		public function index(){
			$kaskeluar_date = date('Y-m-d');
			$this->data['datakaskeluar'] = $this->Mkaskeluar->get_kaskeluar($kaskeluar_date);
			$this->load->view('index',$this->data);
		}

		public function add_transaction(){
			if($this->input->post()){
					$data = $this->input->post();

					$result = $this->Mkaskeluar->add($data);
					if($result === false){echo "something wrong"; exit;}
					redirect($this->data['module']);
			}
			$this->data['dataproduct'] = $this->Mkaskeluar->get_products();
			$this->data['proses'] = "add_transaction";
			$this->data['date'] = date('Y-m-d');
			$this->data['action_form'] = site_url($this->data['module']."/add_transaction");
			$this->load->view('index',$this->data);
		}

		public function update($id){
			if($this->input->post()){
					$data = $this->input->post();
					$this->Mkaskeluar->update($data,$id);
					redirect($this->data['module']);
				}

				$update = $this->Mkaskeluar->get_trx($id);
				$this->data['date'] = tgl_indo(date('Y-m-d'));
				$this->data['dataproduct'] = $update;
				$this->data['trx_id'] = $id;
				$this->data['proses'] = "update";
				$this->load->view('index',$this->data);
		}

		public function delete($id){
			$this->Mkaskeluar->delete($id);
			redirect('Kaskeluar');
		}
	}
