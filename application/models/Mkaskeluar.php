<?php
  class Mkaskeluar extends CI_Model{
    public function  get_kaskeluar($kaskeluar_date){
      $query = $this->db->query("SELECT * FROM kas_keluar a, products b WHERE sales_date = '$kaskeluar_date' AND a.product_id = b.product_id ORDER BY sales_time ASC");
      return $query->result_array();
    }

    public function  get_products(){
      $query = $this->db->query("SELECT * FROM products ORDER BY product ASC");
      return $query->result_array();
    }

    public function  get_trx($id){
      $query = $this->db->query("SELECT a.description, a.sales_qty, a.sales_price, a.sales_stock, b.product, b.product_id FROM kas_keluar a, products b WHERE a.trx_id = '$id' AND a.product_id=b.product_id");
      return $query->row_array();
    }

    private function cek_stock($product){
      	$sql 	= "SELECT * FROM products WHERE product_id = '$product'";
        $data = $this->db->query($sql)->row_array();
        return $data;
    }

    public function add($data){
      $product	= $data['product'];
      $stock = $this->cek_stock($product);
      if(!empty($stock['qty_product_disc']) AND $stock['qty_product_disc']>0){
          $price   = $stock['price'];//ambil harga dari tabel product yang telah di discount sebelumnya
          $sisa_stok_discount = $stock['qty_product_disc'] - $data['qty'];
          $data = array(
                  'qty_product_disc' => $sisa_stok_discount
          );
          //update tabel product untuk perubahan field disc_qty
          $this->db->where('product_id', $id);
          $this->db->update('products', $data);
      }
    	else{
          $price		= $data['harga'];
      }
    	$qty		     = $data['qty'];
    	$description = $data['description'];
      //print_r($stock); exit;
      $subtotal= $price * $qty;
      $profit	= ($price * $qty) - ($stock['po_price'] * $qty);
      $kaskeluar_stock = $stock['stock'] - $qty;
      $kaskeluar_date = date('Y-m-d');
      $hour		= date('H:i:s');
      if($kaskeluar_stock <= 0){
        return json_encode(array('status'=>false, 'msg' => "Stock Habiss"));
        // return false;
      }
      else {
         $sql = "INSERT INTO kas_keluar (	product_id,
     														sales_price,
     														profit,
     														sales_qty,
     														sales_stock,
     														subtotal,
     														sales_date,
     														sales_time,
     														description,
     														user_id)
     												VALUES(	'$product',
     														'$price',
     														'$profit',
     														'$qty',
     														'$kaskeluar_stock',
     														'$subtotal',
     														'$kaskeluar_date',
     														'$hour',
     														'$description',
     														'')";
        $this->db->query($sql);

        $sql2 = "UPDATE products SET stock = '$kaskeluar_stock' WHERE product_id = '$product'";

        $this->db->query($sql2);
      }
    }
    public function update($data,$id){
      $product_id	= $data['product_id'];
    	$price		= $data['harga'];
    	$qty		= $data['qty'];
    	$kaskeluar_qty	= $data['sales_qty'];
    	$description= $data['description'];
    	$trx_id		= $data['trx_id'];
    	$stock		= $data['stock'];

    	$get_product = $this->cek_stock($product_id);
    	$subtotal = $price * $qty;
    	$profit	= ($price * $qty) - ($get_product['po_price'] * $qty);
    	$kaskeluar_stock = ($stock + $kaskeluar_qty) - $qty;


        $sql = "UPDATE kas_keluar SET	sales_price = '$price',
      													profit	= '$profit',
      													sales_qty = '$qty',
      													sales_stock = '$kaskeluar_stock',
      													subtotal	= '$subtotal',
      													description = '$description'
      													WHERE trx_id = '$trx_id'";

      $this->db->query($sql);

      $sql2 = "UPDATE products SET stock = '$kaskeluar_stock' WHERE product_id = '$product_id'";

    $this->db->query($sql2);
    }

    public function delete($id){
      $query = "DELETE FROM kas_keluar WHERE trx_id = '$id'";
      $this->db->query($query);
    }
  }
