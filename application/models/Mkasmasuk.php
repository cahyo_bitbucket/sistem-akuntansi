<?php
  class Mkasmasuk extends CI_Model{
    public function  get_kasmasuk($buys_date){
      $query = $this->db->query("SELECT * FROM kas_masuk a, products b WHERE a.buys_date = '$buys_date' AND a.product_id=b.product_id ORDER BY a.buys_time ASC");
      return $query->result_array();
    }

    public function  get_products(){
      $query = $this->db->query("SELECT * FROM products ORDER BY product ASC");
      return $query->result_array();
    }

    public function  get_masuk($trx_id){
      $query = $this->db->query("SELECT * FROM kas_masuk a, products b WHERE a.trx_id = '$trx_id' AND a.product_id=b.product_id ORDER BY a.buys_time ASC");
      return $query->row_array();
    }
    public function add($data,$qty_old_product){
    	$product	= $data['product'];
    	$price		= $data['harga'];
    	$qty		= $data['qty'];
    	$description= $data['description'];
    	$subtotal	= $price * $qty;
      $buys_date = date('Y-m-d');
      $hour		= date('H:i:s');
      $user = $data['user_id'];
      $sql = "INSERT INTO kas_masuk(product_id,supp_price,supp_qty,subtotal,description,buys_date,buys_time,user_id)
    				VALUES('$product','$price','$qty','$subtotal','$description','$buys_date','$hour','$user')";

      $this->db->query($sql);

      $sisa_stok = $qty_old_product['stock'] + $qty;
      $sql2 = "UPDATE products SET stock = '$sisa_stok', po_price = '$price' WHERE product_id = '$product'";
      $this->db->query($sql2);

    }
    public function update($data,$id,$qty_old_product){
    	$product_id	= $data['product_id'];
    	$price		= $data['harga'];
    	$qty		= $data['qty'];
    	$description= $data['description'];
    	$trx_id		= $data['trx_id'];
    	$buys_qty	= $data['buys_qty'];
    	$subtotal	= $price * $qty;

      $sql = "UPDATE kas_masuk SET 
    												supp_price = '$price',
    												supp_qty = '$qty',
    												subtotal = '$subtotal',
    												description = '$description'
    												WHERE trx_id = '$trx_id'";
      $this->db->query($sql);
      $buys_stock = ($qty_old_product - $buys_qty) + $qty;
      $sql2 = "UPDATE products SET stock = '$buys_stock', po_price = '$price' WHERE product_id = '$product'";
      $this->db->query($sql2);

    }
    //jalankan fungsi dibawah ini dengan cron jobs
    public function get_product_disc(){
      $sql = "SELECT supp_qty,
                     trx_id,
                     product_id,
                     supp_price,
                     DATE_ADD(buys_date,INTERVAL 60 DAY) AS datedisc
              FROM kas_masuk
              WHERE disc = 'Y'";
      $result = $this->db->query($sql);
      if($result->num_rows()>0){
        foreach($result->result_array() as $row){
            $price_disc = $row['supp_price'] - (($row['supp_price']*20)/100);

            //update tabel qty_product_disc di table product
            $data = array(
                    'price'            => $price_disc,
                    'qty_product_disc' => $row['supp_qty']
            );

            $this->db->where('product_id', $row['product_id']);
            $this->db->update('products', $data);

            $data2 = array(
                    'disc'  => "Y"
            );

            $this->db->where('trx_id', $row['trx_id']);
            $this->db->update('products', $data2);
        }
      }
    }

    public function delete($id){
      $query = "DELETE FROM kas_masuk WHERE trx_id = '$id'";
      $this->db->query($query);
    }
  }
